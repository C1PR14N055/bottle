﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// bottleScript
struct bottleScript_t2553388177;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Collision
struct Collision_t2876846408;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// System.Object
struct Il2CppObject;
// bottleScript/<resetBottle>c__Iterator1
struct U3CresetBottleU3Ec__Iterator1_t3674277525;
// bottleScript/<throwBottle>c__Iterator0
struct U3CthrowBottleU3Ec__Iterator0_t3709909167;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// resolution
struct resolution_t2047694894;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_bottleScript2553388177.h"
#include "AssemblyU2DCSharp_bottleScript2553388177MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "AssemblyU2DCSharp_bottleScript_U3CthrowBottleU3Ec_3709909167MethodDeclarations.h"
#include "AssemblyU2DCSharp_bottleScript_U3CthrowBottleU3Ec_3709909167.h"
#include "AssemblyU2DCSharp_bottleScript_U3CresetBottleU3Ec_3674277525MethodDeclarations.h"
#include "AssemblyU2DCSharp_bottleScript_U3CresetBottleU3Ec_3674277525.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collision2876846408MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random1170710517MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Handheld4075775256MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_ForceMode1856518252.h"
#include "AssemblyU2DCSharp_resolution2047694894.h"
#include "AssemblyU2DCSharp_resolution2047694894MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m3347661153_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m3347661153(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3347661153_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.AudioSource>()
#define GameObject_GetComponent_TisAudioSource_t1135106623_m3309832039(__this, method) ((  AudioSource_t1135106623 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3347661153_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, method) ((  Rigidbody_t4233889191 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void bottleScript::.ctor()
extern "C"  void bottleScript__ctor_m3915834402 (bottleScript_t2553388177 * __this, const MethodInfo* method)
{
	{
		__this->set_bottleTiltSpeed_3((2.0f));
		__this->set_bottleMaxTiltAngle_4(((int32_t)45));
		__this->set_bottleThrown_14((bool)0);
		__this->set_reseting_16((bool)0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void bottleScript::Start()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3774118971;
extern const uint32_t bottleScript_Start_m3558569586_MetadataUsageId;
extern "C"  void bottleScript_Start_m3558569586 (bottleScript_t2553388177 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (bottleScript_Start_m3558569586_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3774118971, /*hidden argument*/NULL);
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		__this->set_startPosition_12(L_1);
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Quaternion_t4030073918  L_3 = Transform_get_rotation_m1033555130(L_2, /*hidden argument*/NULL);
		__this->set_startRotation_13(L_3);
		return;
	}
}
// System.Void bottleScript::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t bottleScript_Update_m3577015537_MetadataUsageId;
extern "C"  void bottleScript_Update_m3577015537 (bottleScript_t2553388177 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (bottleScript_Update_m3577015537_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		bool L_0 = __this->get_bottleThrown_14();
		if (L_0)
		{
			goto IL_00d3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_1 = Input_get_acceleration_m2886343410(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = (&V_1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = bankers_roundf(((float)((float)L_2*(float)(100.0f))));
		int32_t L_4 = __this->get_bottleMaxTiltAngle_4();
		V_0 = ((float)((float)((float)((float)((float)((float)L_3/(float)(100.0f)))*(float)(((float)((float)L_4)))))*(float)(-1.0f)));
		Vector3_t2243707580  L_5 = Input_get_acceleration_m2886343410(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = (&V_3)->get_y_2();
		float L_7 = bankers_roundf(((float)((float)L_6*(float)(100.0f))));
		int32_t L_8 = __this->get_bottleMaxTiltAngle_4();
		V_2 = ((float)((float)((float)((float)((float)((float)L_7/(float)(100.0f)))*(float)(((float)((float)L_8)))))*(float)(-1.0f)));
		Vector3_t2243707580  L_9 = Input_get_acceleration_m2886343410(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = L_9;
		float L_10 = (&V_5)->get_z_3();
		float L_11 = bankers_roundf(((float)((float)L_10*(float)(100.0f))));
		int32_t L_12 = __this->get_bottleMaxTiltAngle_4();
		V_4 = ((float)((float)((float)((float)((float)((float)L_11/(float)(100.0f)))*(float)(((float)((float)L_12)))))*(float)(-1.0f)));
		float L_13 = V_4;
		float L_14 = V_2;
		float L_15 = V_0;
		Quaternion_t4030073918  L_16 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
		__this->set_targetRotation_15(L_16);
		Transform_t3275118058 * L_17 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Quaternion_t4030073918  L_19 = Transform_get_rotation_m1033555130(L_18, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_20 = __this->get_targetRotation_15();
		float L_21 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_22 = __this->get_bottleTiltSpeed_3();
		Quaternion_t4030073918  L_23 = Quaternion_Slerp_m1992855400(NULL /*static, unused*/, L_19, L_20, ((float)((float)L_21*(float)L_22)), /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_rotation_m3411284563(L_17, L_23, /*hidden argument*/NULL);
	}

IL_00d3:
	{
		bool L_24 = __this->get_bottleThrown_14();
		if (L_24)
		{
			goto IL_010f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_25 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_25) > ((int32_t)0)))
		{
			goto IL_00f4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_26 = Input_GetMouseButtonDown_m47917805(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_010f;
		}
	}

IL_00f4:
	{
		__this->set_bottleThrown_14((bool)1);
		Il2CppObject * L_27 = bottleScript_throwBottle_m834915237(__this, (0.05f), /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_27, /*hidden argument*/NULL);
	}

IL_010f:
	{
		return;
	}
}
// System.Collections.IEnumerator bottleScript::throwBottle(System.Single)
extern Il2CppClass* U3CthrowBottleU3Ec__Iterator0_t3709909167_il2cpp_TypeInfo_var;
extern const uint32_t bottleScript_throwBottle_m834915237_MetadataUsageId;
extern "C"  Il2CppObject * bottleScript_throwBottle_m834915237 (bottleScript_t2553388177 * __this, float ___time0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (bottleScript_throwBottle_m834915237_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CthrowBottleU3Ec__Iterator0_t3709909167 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CthrowBottleU3Ec__Iterator0_t3709909167 * L_0 = (U3CthrowBottleU3Ec__Iterator0_t3709909167 *)il2cpp_codegen_object_new(U3CthrowBottleU3Ec__Iterator0_t3709909167_il2cpp_TypeInfo_var);
		U3CthrowBottleU3Ec__Iterator0__ctor_m1934922520(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CthrowBottleU3Ec__Iterator0_t3709909167 * L_1 = V_0;
		float L_2 = ___time0;
		NullCheck(L_1);
		L_1->set_time_0(L_2);
		U3CthrowBottleU3Ec__Iterator0_t3709909167 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_3(__this);
		U3CthrowBottleU3Ec__Iterator0_t3709909167 * L_4 = V_0;
		V_1 = L_4;
		goto IL_001b;
	}

IL_001b:
	{
		Il2CppObject * L_5 = V_1;
		return L_5;
	}
}
// System.Collections.IEnumerator bottleScript::resetBottle(System.Single)
extern Il2CppClass* U3CresetBottleU3Ec__Iterator1_t3674277525_il2cpp_TypeInfo_var;
extern const uint32_t bottleScript_resetBottle_m576780010_MetadataUsageId;
extern "C"  Il2CppObject * bottleScript_resetBottle_m576780010 (bottleScript_t2553388177 * __this, float ___time0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (bottleScript_resetBottle_m576780010_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CresetBottleU3Ec__Iterator1_t3674277525 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CresetBottleU3Ec__Iterator1_t3674277525 * L_0 = (U3CresetBottleU3Ec__Iterator1_t3674277525 *)il2cpp_codegen_object_new(U3CresetBottleU3Ec__Iterator1_t3674277525_il2cpp_TypeInfo_var);
		U3CresetBottleU3Ec__Iterator1__ctor_m1271687734(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CresetBottleU3Ec__Iterator1_t3674277525 * L_1 = V_0;
		float L_2 = ___time0;
		NullCheck(L_1);
		L_1->set_time_0(L_2);
		U3CresetBottleU3Ec__Iterator1_t3674277525 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_1(__this);
		U3CresetBottleU3Ec__Iterator1_t3674277525 * L_4 = V_0;
		V_1 = L_4;
		goto IL_001b;
	}

IL_001b:
	{
		Il2CppObject * L_5 = V_1;
		return L_5;
	}
}
// System.Void bottleScript::OnCollisionEnter(UnityEngine.Collision)
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAudioSource_t1135106623_m3309832039_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2201394560;
extern Il2CppCodeGenString* _stringLiteral316635121;
extern Il2CppCodeGenString* _stringLiteral2462112565;
extern const uint32_t bottleScript_OnCollisionEnter_m1069419412_MetadataUsageId;
extern "C"  void bottleScript_OnCollisionEnter_m1069419412 (bottleScript_t2553388177 * __this, Collision_t2876846408 * ___collision0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (bottleScript_OnCollisionEnter_m1069419412_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		AudioSource_t1135106623 * L_1 = GameObject_GetComponent_TisAudioSource_t1135106623_m3309832039(L_0, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t1135106623_m3309832039_MethodInfo_var);
		__this->set_audio_11(L_1);
		Collision_t2876846408 * L_2 = ___collision0;
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Collision_get_relativeVelocity_m2302609283(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = Vector3_get_magnitude_m860342598((&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_4) > ((float)(10.0f)))))
		{
			goto IL_00ff;
		}
	}
	{
		Collision_t2876846408 * L_5 = ___collision0;
		NullCheck(L_5);
		Vector3_t2243707580  L_6 = Collision_get_relativeVelocity_m2302609283(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = Vector3_get_magnitude_m860342598((&V_1), /*hidden argument*/NULL);
		float L_8 = L_7;
		Il2CppObject * L_9 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2201394560, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_11 = Random_Range_m694320887(NULL /*static, unused*/, 1, 5, /*hidden argument*/NULL);
		V_2 = L_11;
		int32_t L_12 = V_2;
		if (((int32_t)((int32_t)L_12-(int32_t)1)) == 0)
		{
			goto IL_0076;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)1)) == 1)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)1)) == 2)
		{
			goto IL_00a4;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)1)) == 3)
		{
			goto IL_00bb;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)1)) == 4)
		{
			goto IL_00d2;
		}
	}
	{
		goto IL_00e9;
	}

IL_0076:
	{
		AudioSource_t1135106623 * L_13 = __this->get_audio_11();
		AudioClip_t1932558630 * L_14 = __this->get_one_5();
		NullCheck(L_13);
		AudioSource_set_clip_m738814682(L_13, L_14, /*hidden argument*/NULL);
		goto IL_00e9;
	}

IL_008d:
	{
		AudioSource_t1135106623 * L_15 = __this->get_audio_11();
		AudioClip_t1932558630 * L_16 = __this->get_two_6();
		NullCheck(L_15);
		AudioSource_set_clip_m738814682(L_15, L_16, /*hidden argument*/NULL);
		goto IL_00e9;
	}

IL_00a4:
	{
		AudioSource_t1135106623 * L_17 = __this->get_audio_11();
		AudioClip_t1932558630 * L_18 = __this->get_three_7();
		NullCheck(L_17);
		AudioSource_set_clip_m738814682(L_17, L_18, /*hidden argument*/NULL);
		goto IL_00e9;
	}

IL_00bb:
	{
		AudioSource_t1135106623 * L_19 = __this->get_audio_11();
		AudioClip_t1932558630 * L_20 = __this->get_four_8();
		NullCheck(L_19);
		AudioSource_set_clip_m738814682(L_19, L_20, /*hidden argument*/NULL);
		goto IL_00e9;
	}

IL_00d2:
	{
		AudioSource_t1135106623 * L_21 = __this->get_audio_11();
		AudioClip_t1932558630 * L_22 = __this->get_five_9();
		NullCheck(L_21);
		AudioSource_set_clip_m738814682(L_21, L_22, /*hidden argument*/NULL);
		goto IL_00e9;
	}

IL_00e9:
	{
		AudioSource_t1135106623 * L_23 = __this->get_audio_11();
		NullCheck(L_23);
		AudioSource_Play_m353744792(L_23, /*hidden argument*/NULL);
		Handheld_Vibrate_m2337207740(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00ff:
	{
		Collision_t2876846408 * L_24 = ___collision0;
		NullCheck(L_24);
		Vector3_t2243707580  L_25 = Collision_get_relativeVelocity_m2302609283(L_24, /*hidden argument*/NULL);
		V_3 = L_25;
		float L_26 = Vector3_get_magnitude_m860342598((&V_3), /*hidden argument*/NULL);
		float L_27 = L_26;
		Il2CppObject * L_28 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral316635121, L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
	}

IL_0123:
	{
		bool L_30 = __this->get_reseting_16();
		if (L_30)
		{
			goto IL_0153;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2462112565, /*hidden argument*/NULL);
		Il2CppObject * L_31 = bottleScript_resetBottle_m576780010(__this, (3.0f), /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_31, /*hidden argument*/NULL);
		__this->set_reseting_16((bool)1);
	}

IL_0153:
	{
		return;
	}
}
// System.Void bottleScript/<resetBottle>c__Iterator1::.ctor()
extern "C"  void U3CresetBottleU3Ec__Iterator1__ctor_m1271687734 (U3CresetBottleU3Ec__Iterator1_t3674277525 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean bottleScript/<resetBottle>c__Iterator1::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CresetBottleU3Ec__Iterator1_MoveNext_m584624194_MetadataUsageId;
extern "C"  bool U3CresetBottleU3Ec__Iterator1_MoveNext_m584624194 (U3CresetBottleU3Ec__Iterator1_t3674277525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CresetBottleU3Ec__Iterator1_MoveNext_m584624194_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0047;
		}
	}
	{
		goto IL_00d7;
	}

IL_0021:
	{
		float L_2 = __this->get_time_0();
		WaitForSeconds_t3839502067 * L_3 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U24current_2(L_3);
		bool L_4 = __this->get_U24disposing_3();
		if (L_4)
		{
			goto IL_0042;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0042:
	{
		goto IL_00d9;
	}

IL_0047:
	{
		bottleScript_t2553388177 * L_5 = __this->get_U24this_1();
		NullCheck(L_5);
		Rigidbody_t4233889191 * L_6 = L_5->get_rb_10();
		NullCheck(L_6);
		Rigidbody_set_useGravity_m2606656539(L_6, (bool)0, /*hidden argument*/NULL);
		bottleScript_t2553388177 * L_7 = __this->get_U24this_1();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(L_7, /*hidden argument*/NULL);
		bottleScript_t2553388177 * L_9 = __this->get_U24this_1();
		NullCheck(L_9);
		Vector3_t2243707580  L_10 = L_9->get_startPosition_12();
		NullCheck(L_8);
		Transform_set_position_m2469242620(L_8, L_10, /*hidden argument*/NULL);
		bottleScript_t2553388177 * L_11 = __this->get_U24this_1();
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(L_11, /*hidden argument*/NULL);
		bottleScript_t2553388177 * L_13 = __this->get_U24this_1();
		NullCheck(L_13);
		Quaternion_t4030073918  L_14 = L_13->get_startRotation_13();
		NullCheck(L_12);
		Transform_set_rotation_m3411284563(L_12, L_14, /*hidden argument*/NULL);
		bottleScript_t2553388177 * L_15 = __this->get_U24this_1();
		NullCheck(L_15);
		Rigidbody_t4233889191 * L_16 = L_15->get_rb_10();
		Vector3_t2243707580  L_17 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		Rigidbody_set_velocity_m2514070071(L_16, L_17, /*hidden argument*/NULL);
		bottleScript_t2553388177 * L_18 = __this->get_U24this_1();
		NullCheck(L_18);
		Rigidbody_t4233889191 * L_19 = L_18->get_rb_10();
		Vector3_t2243707580  L_20 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_19);
		Rigidbody_set_angularVelocity_m824394045(L_19, L_20, /*hidden argument*/NULL);
		bottleScript_t2553388177 * L_21 = __this->get_U24this_1();
		NullCheck(L_21);
		L_21->set_bottleThrown_14((bool)0);
		bottleScript_t2553388177 * L_22 = __this->get_U24this_1();
		NullCheck(L_22);
		L_22->set_reseting_16((bool)0);
		__this->set_U24PC_4((-1));
	}

IL_00d7:
	{
		return (bool)0;
	}

IL_00d9:
	{
		return (bool)1;
	}
}
// System.Object bottleScript/<resetBottle>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CresetBottleU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2233501262 (U3CresetBottleU3Ec__Iterator1_t3674277525 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object bottleScript/<resetBottle>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CresetBottleU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2161849558 (U3CresetBottleU3Ec__Iterator1_t3674277525 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void bottleScript/<resetBottle>c__Iterator1::Dispose()
extern "C"  void U3CresetBottleU3Ec__Iterator1_Dispose_m3529041511 (U3CresetBottleU3Ec__Iterator1_t3674277525 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void bottleScript/<resetBottle>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CresetBottleU3Ec__Iterator1_Reset_m3231879853_MetadataUsageId;
extern "C"  void U3CresetBottleU3Ec__Iterator1_Reset_m3231879853 (U3CresetBottleU3Ec__Iterator1_t3674277525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CresetBottleU3Ec__Iterator1_Reset_m3231879853_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void bottleScript/<throwBottle>c__Iterator0::.ctor()
extern "C"  void U3CthrowBottleU3Ec__Iterator0__ctor_m1934922520 (U3CthrowBottleU3Ec__Iterator0_t3709909167 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean bottleScript/<throwBottle>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody_t4233889191_m520013213_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1374357100;
extern Il2CppCodeGenString* _stringLiteral1931488205;
extern Il2CppCodeGenString* _stringLiteral1931488236;
extern const uint32_t U3CthrowBottleU3Ec__Iterator0_MoveNext_m1510469100_MetadataUsageId;
extern "C"  bool U3CthrowBottleU3Ec__Iterator0_MoveNext_m1510469100 (U3CthrowBottleU3Ec__Iterator0_t3709909167 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CthrowBottleU3Ec__Iterator0_MoveNext_m1510469100_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0047;
		}
	}
	{
		goto IL_011a;
	}

IL_0021:
	{
		float L_2 = __this->get_time_0();
		WaitForSeconds_t3839502067 * L_3 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U24current_4(L_3);
		bool L_4 = __this->get_U24disposing_5();
		if (L_4)
		{
			goto IL_0042;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_0042:
	{
		goto IL_011c;
	}

IL_0047:
	{
		float L_5 = Random_Range_m2884721203(NULL /*static, unused*/, (-1.5f), (1.5f), /*hidden argument*/NULL);
		__this->set_U3CrandomForce1U3E__0_1(L_5);
		float L_6 = Random_Range_m2884721203(NULL /*static, unused*/, (-1.5f), (1.5f), /*hidden argument*/NULL);
		__this->set_U3CrandomForce2U3E__1_2(L_6);
		bottleScript_t2553388177 * L_7 = __this->get_U24this_3();
		bottleScript_t2553388177 * L_8 = __this->get_U24this_3();
		NullCheck(L_8);
		Rigidbody_t4233889191 * L_9 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(L_8, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_MethodInfo_var);
		NullCheck(L_7);
		L_7->set_rb_10(L_9);
		bottleScript_t2553388177 * L_10 = __this->get_U24this_3();
		NullCheck(L_10);
		Rigidbody_t4233889191 * L_11 = L_10->get_rb_10();
		float L_12 = __this->get_U3CrandomForce1U3E__0_1();
		float L_13 = __this->get_U3CrandomForce2U3E__1_2();
		bottleScript_t2553388177 * L_14 = __this->get_U24this_3();
		NullCheck(L_14);
		float L_15 = L_14->get_thrust_2();
		NullCheck(L_11);
		Rigidbody_AddForce_m2352384298(L_11, L_12, L_13, L_15, 1, /*hidden argument*/NULL);
		bottleScript_t2553388177 * L_16 = __this->get_U24this_3();
		NullCheck(L_16);
		Rigidbody_t4233889191 * L_17 = L_16->get_rb_10();
		NullCheck(L_17);
		Rigidbody_set_useGravity_m2606656539(L_17, (bool)1, /*hidden argument*/NULL);
		bottleScript_t2553388177 * L_18 = __this->get_U24this_3();
		NullCheck(L_18);
		float L_19 = L_18->get_thrust_2();
		float L_20 = L_19;
		Il2CppObject * L_21 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1374357100, L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		float L_23 = __this->get_U3CrandomForce1U3E__0_1();
		float L_24 = L_23;
		Il2CppObject * L_25 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_24);
		String_t* L_26 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1931488205, L_25, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		float L_27 = __this->get_U3CrandomForce2U3E__1_2();
		float L_28 = L_27;
		Il2CppObject * L_29 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_28);
		String_t* L_30 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1931488236, L_29, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		__this->set_U24PC_6((-1));
	}

IL_011a:
	{
		return (bool)0;
	}

IL_011c:
	{
		return (bool)1;
	}
}
// System.Object bottleScript/<throwBottle>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CthrowBottleU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1275218196 (U3CthrowBottleU3Ec__Iterator0_t3709909167 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object bottleScript/<throwBottle>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CthrowBottleU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m19915996 (U3CthrowBottleU3Ec__Iterator0_t3709909167 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void bottleScript/<throwBottle>c__Iterator0::Dispose()
extern "C"  void U3CthrowBottleU3Ec__Iterator0_Dispose_m3042436613 (U3CthrowBottleU3Ec__Iterator0_t3709909167 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void bottleScript/<throwBottle>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CthrowBottleU3Ec__Iterator0_Reset_m1140641183_MetadataUsageId;
extern "C"  void U3CthrowBottleU3Ec__Iterator0_Reset_m1140641183 (U3CthrowBottleU3Ec__Iterator0_t3709909167 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CthrowBottleU3Ec__Iterator0_Reset_m1140641183_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void resolution::.ctor()
extern "C"  void resolution__ctor_m3993550373 (resolution_t2047694894 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void resolution::Start()
extern "C"  void resolution_Start_m1057919981 (resolution_t2047694894 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void resolution::Update()
extern "C"  void resolution_Update_m2957515532 (resolution_t2047694894 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
