﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bottleScript/<throwBottle>c__Iterator0
struct U3CthrowBottleU3Ec__Iterator0_t3709909167;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void bottleScript/<throwBottle>c__Iterator0::.ctor()
extern "C"  void U3CthrowBottleU3Ec__Iterator0__ctor_m1934922520 (U3CthrowBottleU3Ec__Iterator0_t3709909167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean bottleScript/<throwBottle>c__Iterator0::MoveNext()
extern "C"  bool U3CthrowBottleU3Ec__Iterator0_MoveNext_m1510469100 (U3CthrowBottleU3Ec__Iterator0_t3709909167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object bottleScript/<throwBottle>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CthrowBottleU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1275218196 (U3CthrowBottleU3Ec__Iterator0_t3709909167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object bottleScript/<throwBottle>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CthrowBottleU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m19915996 (U3CthrowBottleU3Ec__Iterator0_t3709909167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bottleScript/<throwBottle>c__Iterator0::Dispose()
extern "C"  void U3CthrowBottleU3Ec__Iterator0_Dispose_m3042436613 (U3CthrowBottleU3Ec__Iterator0_t3709909167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bottleScript/<throwBottle>c__Iterator0::Reset()
extern "C"  void U3CthrowBottleU3Ec__Iterator0_Reset_m1140641183 (U3CthrowBottleU3Ec__Iterator0_t3709909167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
