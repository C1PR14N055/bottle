﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bottleScript/<resetBottle>c__Iterator1
struct U3CresetBottleU3Ec__Iterator1_t3674277525;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void bottleScript/<resetBottle>c__Iterator1::.ctor()
extern "C"  void U3CresetBottleU3Ec__Iterator1__ctor_m1271687734 (U3CresetBottleU3Ec__Iterator1_t3674277525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean bottleScript/<resetBottle>c__Iterator1::MoveNext()
extern "C"  bool U3CresetBottleU3Ec__Iterator1_MoveNext_m584624194 (U3CresetBottleU3Ec__Iterator1_t3674277525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object bottleScript/<resetBottle>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CresetBottleU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2233501262 (U3CresetBottleU3Ec__Iterator1_t3674277525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object bottleScript/<resetBottle>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CresetBottleU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2161849558 (U3CresetBottleU3Ec__Iterator1_t3674277525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bottleScript/<resetBottle>c__Iterator1::Dispose()
extern "C"  void U3CresetBottleU3Ec__Iterator1_Dispose_m3529041511 (U3CresetBottleU3Ec__Iterator1_t3674277525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bottleScript/<resetBottle>c__Iterator1::Reset()
extern "C"  void U3CresetBottleU3Ec__Iterator1_Reset_m3231879853 (U3CresetBottleU3Ec__Iterator1_t3674277525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
