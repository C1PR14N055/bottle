﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bottleScript
struct bottleScript_t2553388177;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Collision
struct Collision_t2876846408;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"

// System.Void bottleScript::.ctor()
extern "C"  void bottleScript__ctor_m3915834402 (bottleScript_t2553388177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bottleScript::Start()
extern "C"  void bottleScript_Start_m3558569586 (bottleScript_t2553388177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bottleScript::Update()
extern "C"  void bottleScript_Update_m3577015537 (bottleScript_t2553388177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator bottleScript::throwBottle(System.Single)
extern "C"  Il2CppObject * bottleScript_throwBottle_m834915237 (bottleScript_t2553388177 * __this, float ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator bottleScript::resetBottle(System.Single)
extern "C"  Il2CppObject * bottleScript_resetBottle_m576780010 (bottleScript_t2553388177 * __this, float ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bottleScript::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void bottleScript_OnCollisionEnter_m1069419412 (bottleScript_t2553388177 * __this, Collision_t2876846408 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
