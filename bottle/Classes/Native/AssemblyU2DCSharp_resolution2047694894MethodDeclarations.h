﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// resolution
struct resolution_t2047694894;

#include "codegen/il2cpp-codegen.h"

// System.Void resolution::.ctor()
extern "C"  void resolution__ctor_m3993550373 (resolution_t2047694894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void resolution::Start()
extern "C"  void resolution_Start_m1057919981 (resolution_t2047694894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void resolution::Update()
extern "C"  void resolution_Update_m2957515532 (resolution_t2047694894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
