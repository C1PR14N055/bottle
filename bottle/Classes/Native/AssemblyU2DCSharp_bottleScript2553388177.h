﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// bottleScript
struct  bottleScript_t2553388177  : public MonoBehaviour_t1158329972
{
public:
	// System.Single bottleScript::thrust
	float ___thrust_2;
	// System.Single bottleScript::bottleTiltSpeed
	float ___bottleTiltSpeed_3;
	// System.Int32 bottleScript::bottleMaxTiltAngle
	int32_t ___bottleMaxTiltAngle_4;
	// UnityEngine.AudioClip bottleScript::one
	AudioClip_t1932558630 * ___one_5;
	// UnityEngine.AudioClip bottleScript::two
	AudioClip_t1932558630 * ___two_6;
	// UnityEngine.AudioClip bottleScript::three
	AudioClip_t1932558630 * ___three_7;
	// UnityEngine.AudioClip bottleScript::four
	AudioClip_t1932558630 * ___four_8;
	// UnityEngine.AudioClip bottleScript::five
	AudioClip_t1932558630 * ___five_9;
	// UnityEngine.Rigidbody bottleScript::rb
	Rigidbody_t4233889191 * ___rb_10;
	// UnityEngine.AudioSource bottleScript::audio
	AudioSource_t1135106623 * ___audio_11;
	// UnityEngine.Vector3 bottleScript::startPosition
	Vector3_t2243707580  ___startPosition_12;
	// UnityEngine.Quaternion bottleScript::startRotation
	Quaternion_t4030073918  ___startRotation_13;
	// System.Boolean bottleScript::bottleThrown
	bool ___bottleThrown_14;
	// UnityEngine.Quaternion bottleScript::targetRotation
	Quaternion_t4030073918  ___targetRotation_15;
	// System.Boolean bottleScript::reseting
	bool ___reseting_16;

public:
	inline static int32_t get_offset_of_thrust_2() { return static_cast<int32_t>(offsetof(bottleScript_t2553388177, ___thrust_2)); }
	inline float get_thrust_2() const { return ___thrust_2; }
	inline float* get_address_of_thrust_2() { return &___thrust_2; }
	inline void set_thrust_2(float value)
	{
		___thrust_2 = value;
	}

	inline static int32_t get_offset_of_bottleTiltSpeed_3() { return static_cast<int32_t>(offsetof(bottleScript_t2553388177, ___bottleTiltSpeed_3)); }
	inline float get_bottleTiltSpeed_3() const { return ___bottleTiltSpeed_3; }
	inline float* get_address_of_bottleTiltSpeed_3() { return &___bottleTiltSpeed_3; }
	inline void set_bottleTiltSpeed_3(float value)
	{
		___bottleTiltSpeed_3 = value;
	}

	inline static int32_t get_offset_of_bottleMaxTiltAngle_4() { return static_cast<int32_t>(offsetof(bottleScript_t2553388177, ___bottleMaxTiltAngle_4)); }
	inline int32_t get_bottleMaxTiltAngle_4() const { return ___bottleMaxTiltAngle_4; }
	inline int32_t* get_address_of_bottleMaxTiltAngle_4() { return &___bottleMaxTiltAngle_4; }
	inline void set_bottleMaxTiltAngle_4(int32_t value)
	{
		___bottleMaxTiltAngle_4 = value;
	}

	inline static int32_t get_offset_of_one_5() { return static_cast<int32_t>(offsetof(bottleScript_t2553388177, ___one_5)); }
	inline AudioClip_t1932558630 * get_one_5() const { return ___one_5; }
	inline AudioClip_t1932558630 ** get_address_of_one_5() { return &___one_5; }
	inline void set_one_5(AudioClip_t1932558630 * value)
	{
		___one_5 = value;
		Il2CppCodeGenWriteBarrier(&___one_5, value);
	}

	inline static int32_t get_offset_of_two_6() { return static_cast<int32_t>(offsetof(bottleScript_t2553388177, ___two_6)); }
	inline AudioClip_t1932558630 * get_two_6() const { return ___two_6; }
	inline AudioClip_t1932558630 ** get_address_of_two_6() { return &___two_6; }
	inline void set_two_6(AudioClip_t1932558630 * value)
	{
		___two_6 = value;
		Il2CppCodeGenWriteBarrier(&___two_6, value);
	}

	inline static int32_t get_offset_of_three_7() { return static_cast<int32_t>(offsetof(bottleScript_t2553388177, ___three_7)); }
	inline AudioClip_t1932558630 * get_three_7() const { return ___three_7; }
	inline AudioClip_t1932558630 ** get_address_of_three_7() { return &___three_7; }
	inline void set_three_7(AudioClip_t1932558630 * value)
	{
		___three_7 = value;
		Il2CppCodeGenWriteBarrier(&___three_7, value);
	}

	inline static int32_t get_offset_of_four_8() { return static_cast<int32_t>(offsetof(bottleScript_t2553388177, ___four_8)); }
	inline AudioClip_t1932558630 * get_four_8() const { return ___four_8; }
	inline AudioClip_t1932558630 ** get_address_of_four_8() { return &___four_8; }
	inline void set_four_8(AudioClip_t1932558630 * value)
	{
		___four_8 = value;
		Il2CppCodeGenWriteBarrier(&___four_8, value);
	}

	inline static int32_t get_offset_of_five_9() { return static_cast<int32_t>(offsetof(bottleScript_t2553388177, ___five_9)); }
	inline AudioClip_t1932558630 * get_five_9() const { return ___five_9; }
	inline AudioClip_t1932558630 ** get_address_of_five_9() { return &___five_9; }
	inline void set_five_9(AudioClip_t1932558630 * value)
	{
		___five_9 = value;
		Il2CppCodeGenWriteBarrier(&___five_9, value);
	}

	inline static int32_t get_offset_of_rb_10() { return static_cast<int32_t>(offsetof(bottleScript_t2553388177, ___rb_10)); }
	inline Rigidbody_t4233889191 * get_rb_10() const { return ___rb_10; }
	inline Rigidbody_t4233889191 ** get_address_of_rb_10() { return &___rb_10; }
	inline void set_rb_10(Rigidbody_t4233889191 * value)
	{
		___rb_10 = value;
		Il2CppCodeGenWriteBarrier(&___rb_10, value);
	}

	inline static int32_t get_offset_of_audio_11() { return static_cast<int32_t>(offsetof(bottleScript_t2553388177, ___audio_11)); }
	inline AudioSource_t1135106623 * get_audio_11() const { return ___audio_11; }
	inline AudioSource_t1135106623 ** get_address_of_audio_11() { return &___audio_11; }
	inline void set_audio_11(AudioSource_t1135106623 * value)
	{
		___audio_11 = value;
		Il2CppCodeGenWriteBarrier(&___audio_11, value);
	}

	inline static int32_t get_offset_of_startPosition_12() { return static_cast<int32_t>(offsetof(bottleScript_t2553388177, ___startPosition_12)); }
	inline Vector3_t2243707580  get_startPosition_12() const { return ___startPosition_12; }
	inline Vector3_t2243707580 * get_address_of_startPosition_12() { return &___startPosition_12; }
	inline void set_startPosition_12(Vector3_t2243707580  value)
	{
		___startPosition_12 = value;
	}

	inline static int32_t get_offset_of_startRotation_13() { return static_cast<int32_t>(offsetof(bottleScript_t2553388177, ___startRotation_13)); }
	inline Quaternion_t4030073918  get_startRotation_13() const { return ___startRotation_13; }
	inline Quaternion_t4030073918 * get_address_of_startRotation_13() { return &___startRotation_13; }
	inline void set_startRotation_13(Quaternion_t4030073918  value)
	{
		___startRotation_13 = value;
	}

	inline static int32_t get_offset_of_bottleThrown_14() { return static_cast<int32_t>(offsetof(bottleScript_t2553388177, ___bottleThrown_14)); }
	inline bool get_bottleThrown_14() const { return ___bottleThrown_14; }
	inline bool* get_address_of_bottleThrown_14() { return &___bottleThrown_14; }
	inline void set_bottleThrown_14(bool value)
	{
		___bottleThrown_14 = value;
	}

	inline static int32_t get_offset_of_targetRotation_15() { return static_cast<int32_t>(offsetof(bottleScript_t2553388177, ___targetRotation_15)); }
	inline Quaternion_t4030073918  get_targetRotation_15() const { return ___targetRotation_15; }
	inline Quaternion_t4030073918 * get_address_of_targetRotation_15() { return &___targetRotation_15; }
	inline void set_targetRotation_15(Quaternion_t4030073918  value)
	{
		___targetRotation_15 = value;
	}

	inline static int32_t get_offset_of_reseting_16() { return static_cast<int32_t>(offsetof(bottleScript_t2553388177, ___reseting_16)); }
	inline bool get_reseting_16() const { return ___reseting_16; }
	inline bool* get_address_of_reseting_16() { return &___reseting_16; }
	inline void set_reseting_16(bool value)
	{
		___reseting_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
