﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Unity;
using Firebase.Unity.Editor;
using Firebase.Database;
using ImageAndVideoPicker;

public class ManagerScript : MonoBehaviour {

	// SCORE 
	private int score = 0;
	private const string SCORE_TAG = "SCORE"; 
	public Text scoreText;
	public Text log;

	// OBJECT SETTINGS
	public GameObject throwableObject;
	public float respawnObjectTime;
	public Vector3 instancePosition;
	public Quaternion instanceRotation;

	// MENU
	public GameObject menuCanvas;
	public GameObject overlayCanvas;
	private bool gameIsPlaying = true;

	// OBJECT TO BE THROWN
	private GameObject throwableInstance;
	private bool respawningObject = false;

	// USER INFO
	private const string HAS_POSTED_INFO = "HAS_POSTED_INFO";

	//Firebase db reference
	private DatabaseReference fireDatabase;

	//UNIQUE DEVICE ID
	private string uniqueDeviceId;

	void OnEnable()
	{
		PickerEventListener.onImageLoad += OnImageLoad;
	}

	// Use this for initialization
	void Start () {
		//Don't sleep please
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		uniqueDeviceId = SystemInfo.deviceUniqueIdentifier;
		initFirebase ();
		updateUserInfo ();
		getUserInfo ();
		score = PlayerPrefs.GetInt(SCORE_TAG);
		instantiateThrowable ();
	}
	
	// Update is called once per frame
	void Update () {
		scoreText.text = "Smashed: " + score.ToString ();
	}

	void initFirebase() {
		//Firebase.FirebaseApp app = Firebase.FirebaseApp.DefaultInstance;
		Firebase.FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://bottle-b7260.firebaseio.com/");
		fireDatabase = FirebaseDatabase.DefaultInstance.RootReference;
	}

	void getUserInfo() {
		FirebaseDatabase.DefaultInstance
			.GetReference("users")
			.GetValueAsync().ContinueWith(task => {
				if (task.IsFaulted) {
					Debug.Log("------- GET FAILED -------");
				}
				else if (task.IsCompleted) {
					DataSnapshot snapshot = task.Result;
					try {
						Debug.Log("------- GET -------");
						Debug.Log("> SNAP: " + snapshot.Child(uniqueDeviceId).Child("score").Value);
						int FBScore = int.Parse(snapshot.Child(uniqueDeviceId).Child("score").GetRawJsonValue());
						Debug.Log("> GET VAL: " + FBScore);
						if (FBScore != PlayerPrefs.GetInt(SCORE_TAG)) {
							PlayerPrefs.SetInt(SCORE_TAG, FBScore);
							score = PlayerPrefs.GetInt(SCORE_TAG);
						}
						Debug.Log("------- /GET -------");
					} catch (UnityException ue) {
						Debug.Log("------- GET FAILED -------");
						Debug.Log(ue.Message);
						Debug.Log("------- /GET FAILED -------");
					}
				}
			});
	}

	void updateUserInfo(){
		if (PlayerPrefs.GetInt (HAS_POSTED_INFO, 0) == 0) {
			fireDatabase.Child ("users").Child (uniqueDeviceId).Child ("platform").SetValueAsync (SystemInfo.operatingSystem);
			fireDatabase.Child ("users").Child (uniqueDeviceId).Child ("deviceModel").SetValueAsync (SystemInfo.deviceModel);
			fireDatabase.Child ("users").Child (uniqueDeviceId).Child ("deviceName").SetValueAsync (SystemInfo.deviceName);
			PlayerPrefs.SetInt (HAS_POSTED_INFO, 1);
		}
	}

	public void respawnObject() {
		if (!respawningObject) {
			StartCoroutine(waitAndRespawnObject());
			respawningObject = true;
		}
	}

	IEnumerator waitAndRespawnObject() {
		yield return new WaitForSeconds(respawnObjectTime);
		instantiateThrowable();
		respawningObject = false;
	}

	public void instantiateThrowable() {
		throwableInstance = Instantiate (throwableObject, instancePosition, instanceRotation);
	}

	public void AddToScore(int x) {
		score += x;
		PlayerPrefs.SetInt(SCORE_TAG, score);
		if (true || score % 1 == 0) { // to adjust depending on nr of users
			fireDatabase.Child("users").Child(uniqueDeviceId).Child("score").SetValueAsync(score);
		}
	}

	public void showMenu(){
		gameIsPlaying = false;
		menuCanvas.SetActive(true);
		overlayCanvas.SetActive(false);
	}

	public void hideMenu(){
		gameIsPlaying = true;
		menuCanvas.SetActive(false);
		overlayCanvas.SetActive(true);
	}

	public bool getGameIsPlaying() {
		return gameIsPlaying;
	}

	public void loadPicture() {
		AndroidPicker.BrowseImage ();
	}

	public void takePicture(){
		WebCamTexture webCamTexture = new WebCamTexture();
		webCamTexture.Play();
		Texture2D PhotoTaken = new Texture2D(webCamTexture.width, webCamTexture.height);
		PhotoTaken.SetPixels(webCamTexture.GetPixels());
		PhotoTaken.Apply();
	}
		
	void OnImageLoad(string imgPath, Texture2D tex, ImageAndVideoPicker.ImageOrientation orientation )
	{
		// imgPath : borwsed image path 
		// tex : image texture
		Debug.Log("IMGPATH: " + imgPath);
		log.text = imgPath;
		try {
			GameObject poster = GameObject.FindWithTag("Poster");
			Material material = new Material(Shader.Find("Diffuse"));
			material.mainTexture = tex;
			poster.GetComponent<Renderer> ().material = material;
			Debug.Log("----------- IMG LOADED ---------");
		} catch (UnityException ue) {
			Debug.Log("----------- IMG NOT LOADED :( ---------");
			Debug.Log (ue.Message);
			Debug.Log("----------- IMG NOT LOADED :( ---------");
		}

	}
}
