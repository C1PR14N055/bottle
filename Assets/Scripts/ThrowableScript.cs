﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class ThrowableScript : MonoBehaviour {

	private ManagerScript managerScript;

	public float thrust;
	public float objectTiltSpeed;
	public int objectMaxTiltAngle;
	public GameObject destroyedObject;
	public bool destroyThrownObject;
	public float minExplodeMagnitude;
	public AudioClip one;
	public AudioClip two;
	public AudioClip three;
	public AudioClip four;
	public AudioClip five; 
	public GameObject crack;

	private GameObject throwableInstance;
	private Vector3 initialScale;
	private Vector3 initialPosition;
	private Rigidbody rigidBody;
	private bool objectThrown = false;
	private Quaternion targetRotation;

	private bool didAddToScore = false;
	private bool reseting = false;
	
	// Use this for initialization
	void Start () {
		Debug.Log("EXEC");
		managerScript = GameObject.FindGameObjectWithTag("GameController").GetComponent<ManagerScript>(); //aka GOD
		rigidBody = GetComponent<Rigidbody>();
		initialScale = transform.localScale;
		initialPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log("X:" + Input.acceleration.x + " Y:" + Input.acceleration.y + " Z:" + Input.acceleration.z);
		if (managerScript.getGameIsPlaying() && !objectThrown) {
			float x = -1 * (180 - Mathf.Round(Input.acceleration.x * 100f) / 100f * objectMaxTiltAngle * -1);
			float y = Mathf.Round(Input.acceleration.y * 100f) / 100f * objectMaxTiltAngle * -1;
			float z = Mathf.Round(Input.acceleration.z * 100f) / 100f * objectMaxTiltAngle * -1;
			targetRotation = Quaternion.Euler (z, y, x);
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * objectTiltSpeed);

			// Slide sideways code, has problems
			// Debug.Log("INITIAL POS:" + initialPosition.x);
			// Debug.Log("TRANSF POS:" + transform.position.x);
			// if (Mathf.Abs(initialPosition.x - transform.position.x) < 0.1f) {
			// 	transform.Translate(Vector3.right * 0.01f);
			// }

		}
	}

	void FixedUpdate() {
		if (managerScript.getGameIsPlaying() && !objectThrown) {
			#if UNITY_EDITOR
				if (Input.touchCount > 0 || Input.GetMouseButtonDown(0)) {
					objectThrown = true;
					float throwForce = 3.0f;
					throwObject(throwForce);
				}
			#endif
			//if (Input.acceleration.x > 1.7f || Input.acceleration.y > 1.7f || Input.acceleration.z > 1.7f) {
			if (Input.acceleration.x < -1.7f) {
				objectThrown = true;
				float throwForce = Mathf.Max (Mathf.Max (Mathf.Abs(Input.acceleration.x), Mathf.Abs(Input.acceleration.y)), Mathf.Abs(Input.acceleration.z));
				throwObject(throwForce);
			}
		}
	}

	void throwObject(float force) {	 
		// TODO random force should be proportional to force param
		float randomForce1 = Random.Range(-0.7f, 0.7f);
		float randomForce2 = Random.Range(-0.7f, 0.7f);
		rigidBody.AddForce(randomForce1, randomForce2, thrust * force, ForceMode.Impulse);
		rigidBody.useGravity = true;
	}

	IEnumerator destroyObject(Vector3 previousVelocity){
		yield return new WaitForSeconds (0f);
	}

	void OnCollisionEnter(Collision collision) {

		if (collision.gameObject.tag == "Wall") {
			//TODO different cracks and sizes
			Vector3 newCrackPoint = collision.contacts[0].point;
			newCrackPoint[2] = -3.44f; //dumb fix for collision behind wall
			Vector3 randomRotation = new Vector3(Random.Range(0, 359), -90, 90);
			GameObject crackObject = Instantiate(crack, newCrackPoint, Quaternion.Euler(randomRotation));
			float crackScale = collision.relativeVelocity.magnitude / 15.0f;
			if (crackScale > 2) {
				crackScale = 2;
			}
			crackObject.transform.localScale = crackObject.transform.localScale * crackScale;
		}
			
		if (collision.relativeVelocity.magnitude > minExplodeMagnitude) {
			Vector3 previousVelocity = rigidBody.GetPointVelocity(transform.position);
			Debug.Log("BIG COLL: " + collision.relativeVelocity.magnitude);
			switch (Random.Range(1, 5)) {
				case 1: {
					AudioSource.PlayClipAtPoint (one, transform.position);
					break;
				}
				case 2: {
					AudioSource.PlayClipAtPoint (two, transform.position);
					break;
				}
				case 3: {
					AudioSource.PlayClipAtPoint (three, transform.position);
					break;
				}
				case 4: {
					AudioSource.PlayClipAtPoint (four, transform.position);
					break;
				}
				case 5: {
					AudioSource.PlayClipAtPoint (five, transform.position);
					break;
				}
			}

			if(destroyedObject) {
				GameObject instance = Instantiate(destroyedObject, transform.position, transform.rotation);
				instance.transform.localScale = initialScale;
				instance.GetComponent<Rigidbody>().AddForce(previousVelocity, ForceMode.Impulse);
				if (destroyThrownObject) {
					Destroy(instance, 6); // Destroy cracks in Xs
				}
			}
			Destroy(gameObject);

			Handheld.Vibrate();
			if (!didAddToScore) {
				managerScript.AddToScore (1);
				didAddToScore = true;
			}
		} else {
			//Debug.Log("SMALL COLL: " + collision.relativeVelocity.magnitude);
			Destroy(gameObject, 6); // Destroy unbroken bottle in Xs
		}

		managerScript.respawnObject();

	}
}	